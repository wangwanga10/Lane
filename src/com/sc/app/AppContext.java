package com.sc.app;

import android.app.Application;

public class AppContext extends Application{
	private int mAttachedPointId;

	public boolean isLogin() {
		return false;
	}

	public void setPhotoAttachedPointId(int pointId){
		mAttachedPointId = pointId;
	}
	
	public int getPhotoAttachedPointId(){
		return mAttachedPointId;
	}
}
