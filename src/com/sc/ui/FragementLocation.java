package com.sc.ui;

import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.map.BaiduMap.OnMarkerClickListener;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.geocode.GeoCoder;
import com.baidu.mapapi.search.geocode.ReverseGeoCodeOption;
import com.baidu.mapapi.utils.CoordinateConverter;
import com.baidu.mapapi.utils.CoordinateConverter.CoordType;
import com.sc.app.LocationMarkType;
import com.sc.app.SPStorage;
import com.sc.common.UIHelper;
import com.sc.db.DBStorage;
import com.sc.lane.R;
import com.sc.map.LaneSearchListener;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class FragementLocation extends Fragment{	
	private MapView mMapView = null;
	private BaiduMap mMap;
	private Marker mMarkerA;
	private Context mContext;
	private BitmapDescriptor mCurrentMarker;
	private LocationDataReceiver receiver ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	mContext = getActivity().getApplicationContext();
    	// 在使用SDK各组件之前初始化context信息，传入ApplicationContext
    	// 注意该方法要再setContentView方法之前实现
    	SDKInitializer.initialize(mContext);
    	View view = inflater.inflate(R.layout.main_fragement_loc, container, false);
        // 获取地图控件引用
 		mMapView = (MapView) view.findViewById(R.id.bmapView);
 		mMap = mMapView.getMap();
 		mCurrentMarker = BitmapDescriptorFactory  
			    .fromResource(R.drawable.location_marker);
 		MapStatusUpdate msu = MapStatusUpdateFactory.zoomTo(14.0f);
 		mMap.setMapStatus(msu);
 		
 		mMap.setOnMarkerClickListener(new OnMarkerClickListener() {

			@Override
			public boolean onMarkerClick(Marker arg0) {
				LatLng pos = mMarkerA.getPosition();
				
				ActionSelecitonListener listener = new ActionSelecitonListener(pos);
				
				new AlertDialog.Builder(getActivity()).setTitle("选择操作").setItems(
					     new String[] { "起点", "沿途标记点", "终点", "取消本次记录"}, listener).setNegativeButton(
					     "取消", null).show();
				return true;
			}
 		});

 		receiver = new LocationDataReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction("android.intent.action.loc");
		this.getActivity().registerReceiver(receiver, filter);
		
		Intent intent = new Intent("com.sc.service.LocationService"); 
		this.getActivity().startService(intent);
		
        return view;
    }

	@Override
	public void onDestroy() {
		super.onDestroy();
		// 在activity执行onDestroy时执行mMapView.onDestroy()，实现地图生命周期管理
		mMapView.onDestroy();
		mCurrentMarker.recycle();
		
		this.getActivity().unregisterReceiver(receiver);
		
		Intent intent = new Intent("com.sc.service.LocationService"); 
		this.getActivity().stopService(intent);
	}

	@Override
	public void onResume() {
		super.onResume();
		// 在activity执行onResume时执行mMapView. onResume ()，实现地图生命周期管理
		mMapView.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
		// 在activity执行onPause时执行mMapView. onPause ()，实现地图生命周期管理
		mMapView.onPause();
	}
	
	public class LocationDataReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context arg0, Intent arg1) {
			System.out.println("OnReceiver");
			
			Location location = (Location) arg1.getParcelableExtra("loc");
			
			updateOverlay(location);
		}
	}

	public void updateOverlay(Location location) {  
		if(location != null){
			// 清空地图所有的 Overlay 覆盖物以及 InfoWindow
			mMap.clear();
			
			// add marker overlay
			// 将GPS设备采集的原始GPS坐标转换成百度坐标  
			CoordinateConverter converter  = new CoordinateConverter();  
			converter.from(CoordType.GPS);  
			LatLng sourceLatLng = new LatLng(location.getLatitude(), location.getLongitude());
			// sourceLatLng待转换坐标  
			converter.coord(sourceLatLng);  
			LatLng ll = converter.convert();
			OverlayOptions ooA = new MarkerOptions().position(ll).icon(mCurrentMarker)
					.zIndex(9).draggable(false);
			mMarkerA = (Marker) (mMap.addOverlay(ooA));
			MapStatusUpdate msu = MapStatusUpdateFactory.newLatLng(ll);
	 		mMap.setMapStatus(msu);
		}
	}

	public void clearOverlay(View view) {
		mMap.clear();
	}
	
	public class ActionSelecitonListener implements DialogInterface.OnClickListener{
		private LatLng pos;
		
		public ActionSelecitonListener(LatLng pos){
			this.pos = pos;
		}
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			Log.i("info", "你点击了" + which);
			int previousMark = SPStorage.getCurrentMarkType(mContext);
			switch(which){
			case LocationMarkType.MARK_AS_BEGIN:
				if(previousMark == LocationMarkType.MARK_AS_END || 
						previousMark == LocationMarkType.MARK_CANCEL ||
								previousMark == LocationMarkType.MARK_UNDEFINE){
					SPStorage.setCurrentMarkType(mContext, LocationMarkType.MARK_AS_BEGIN);
					
					/* 尝试通过反向地理解析，获取地理位置。监听器的回调函数会创建一个记录。 */
					GeoCoder coder = GeoCoder.newInstance();
					coder.setOnGetGeoCodeResultListener(new LaneSearchListener(mContext, 
							pos, LocationMarkType.MARK_AS_BEGIN));
					coder.reverseGeoCode(new ReverseGeoCodeOption().location(pos));
				}
				else
				{
					UIHelper.ToastMessage(mContext, 
							mContext.getResources().getString(R.string.location_mark_in_progess));
				}
				break;
			case LocationMarkType.MARK_AS_MIDDLE:
				if(previousMark == LocationMarkType.MARK_AS_BEGIN ||
						previousMark == LocationMarkType.MARK_AS_MIDDLE)
				{
					SPStorage.setCurrentMarkType(mContext, LocationMarkType.MARK_AS_MIDDLE);
					
					/* 尝试通过反向地理解析，获取地理位置。监听器的回调函数会创建一个记录。 */
					GeoCoder coder = GeoCoder.newInstance();
					coder.setOnGetGeoCodeResultListener(new LaneSearchListener(mContext, 
							pos, LocationMarkType.MARK_AS_MIDDLE));
					coder.reverseGeoCode(new ReverseGeoCodeOption().location(pos));
				}
				else{
					UIHelper.ToastMessage(mContext, 
							mContext.getResources().getString(R.string.location_mark_not_in_progess));
				}
				break;
			case LocationMarkType.MARK_AS_END:
				if(previousMark == LocationMarkType.MARK_AS_BEGIN ||
						previousMark == LocationMarkType.MARK_AS_MIDDLE)
				{
					SPStorage.setCurrentMarkType(mContext, LocationMarkType.MARK_AS_END);
					
					/* 尝试通过反向地理解析，获取地理位置。监听器的回调函数会创建一个记录。 */
					GeoCoder coder = GeoCoder.newInstance();
					coder.setOnGetGeoCodeResultListener(new LaneSearchListener(mContext, 
							pos, LocationMarkType.MARK_AS_END));
					coder.reverseGeoCode(new ReverseGeoCodeOption().location(pos));
				}
				else{
					UIHelper.ToastMessage(mContext, 
							mContext.getResources().getString(R.string.location_mark_not_in_progess));
				}
				break;
			case LocationMarkType.MARK_CANCEL:
				SPStorage.setCurrentMarkType(mContext, LocationMarkType.MARK_CANCEL);
				
				DBStorage.removeMarkRecord(mContext, SPStorage.getCurrentRecordId(mContext));
				
				SPStorage.setCurrentRecordId(mContext, 0);
				break;
			}
		}
	}
}


