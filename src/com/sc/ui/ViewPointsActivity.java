package com.sc.ui;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ListView;

import com.sc.adapter.PointsAdapter;
import com.sc.app.AppContext;
import com.sc.db.DBStorage;
import com.sc.db.MarkPoint;
import com.sc.lane.R;
import com.sc.utils.FileUtil;

public class ViewPointsActivity extends Activity{
	private ListView mlvPointsList;
	private List<MarkPoint> mPointsList = null;
	private PointsAdapter mAdapter;
	private Activity mActivity;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_record_points);
		mlvPointsList = (ListView)this.findViewById(R.id.lv_points);
		
		this.mActivity = this;
		Intent intent = this.getIntent();
		int recordId = intent.getIntExtra("record_id", 0);
		mPointsList = DBStorage.getMarkPoints(mActivity, recordId);
		mAdapter = new PointsAdapter(this, mPointsList);
		mlvPointsList.setAdapter(mAdapter);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
		case 1:
			if (data != null) {
				//取得返回的Uri,基本上选择照片的时候返回的是以Uri形式，但是在拍照中有得机子呢Uri是空的，所以要特别注意
				Uri mImageCaptureUri = data.getData();
				//返回的Uri不为空时，那么图片信息数据都会在Uri中获得。如果为空，那么我们就进行下面的方式获取
				if (mImageCaptureUri != null) {
					Bitmap bitmap;
					try {
						//这个方法是根据Uri获取Bitmap图片的静态方法
						bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), mImageCaptureUri);
						savePhoto(bitmap);
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					Bundle extras = data.getExtras();
					if (extras != null) {
						//这里是有些拍照后的图片是直接存放到Bundle中的所以我们可以从这里面获取Bitmap图片
						Bitmap bitmap = extras.getParcelable("data");
						savePhoto(bitmap);
					}
				}
			}
			break;
		default:
			break;
		}
	}
	
	private void savePhoto(Bitmap bitmap)
	{
		if(bitmap != null){
			String fileName = String.valueOf(System.currentTimeMillis()) + ".jpg";
			FileUtil.savePicToSdcard(bitmap, fileName);
			
			int pointId = ((AppContext)mActivity.getApplicationContext()).getPhotoAttachedPointId();
			if(!DBStorage.updateMarkPointPhoto(mActivity, pointId, fileName)){
				Log.e("error", "update point photo failed!");
			}
		}
	}
}
