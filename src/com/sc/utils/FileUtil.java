package com.sc.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import com.sc.app.Config;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.util.Log;

public class FileUtil {
	public static String savePicToSdcard(Bitmap bitmap, String fileName) {
		String filePath = "";
		
		File folder = new File(Config.PHOTO_FOLDER);
		if(!folder.exists()){
			folder.mkdir();
		}
		
		if (bitmap == null) {
			return filePath;
		} else {
			filePath = Config.PHOTO_FOLDER + fileName;
			File destFile = new File(filePath);
			OutputStream os = null;
			try {
				os = new FileOutputStream(destFile);
				bitmap.compress(CompressFormat.JPEG, 100, os);
				os.flush();
				os.close();
			} catch (IOException e) {
				filePath = "";
			}
		}
		
		return filePath;
	}

	public static boolean isDBFileExist() {
		String path = com.sc.app.Config.DATABASE.PATH + "databases"
				+ File.separator;
		String DB_NAME = path + "DB.db";
		File file = new File(DB_NAME);
		long length = file.length();
		if (file.exists()) {

			if (length < 10240) {
				file.delete();
				return false;
			}
			return true;
		} else {
			return false;
		}
	}

	public static String getFilePath() {
		String path = Config.DATABASE.PATH + "databases" + File.separator;
		String DB_NAME = path + "DB.db";
		Log.d("DATABASE_FILE", "创建数据库文件！");
		File dir = new File(path);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		return DB_NAME;
	}
}
